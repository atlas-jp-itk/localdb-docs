# Technicalities

## Chip Configs

### Where your chip config is recorded

In order to understand the workflow, you need to be aware of that there are __3 places__ that your chip config is recorded.

* Production database (ITkPD)
* LocalDB
* Your local DAQ machine (DAQ host)

Within the `LocalDB v2.2` and `QC_v2` scheme, the flow of config downloading/uploading and its revision is adminstrated as follows:

* The DAQ host __generates__ or __pulls__ the chip config interacting with ITkPD.
* LocalDB __records__ the local revision of the chip config, and (only) the representative chip config of the stage (after Tuning) is __submitted__ to ITkPD.
* Not all local revisions of the config is shared with ITkPD. In that sense, the record on ITkPD should be regarded as the config __sharable__ with other sites.
* The config in DAQ host evolves as the scan progresses (the same config filename is overwritten by every single scan). Past config is recorded in each scan result data directory.

!!! note
	In the future release, this config should not persist within the DAQ host where the user will handle in the standard workflow, and instead the adequate config is planned to be pulled from LocalDB "on the fly".

### Management of FE chip config in LocalDB

FE chip config is a `JSON` data containing parameters to steer a FE chip. Three major blocks are present in a FE chip config:

* `GlobalConfig`: For a ITkPix chip, approximately 200 global parameters are present. These corresponds to the chip's global registers.
* `Parameter`: External parameter values e.g. temperature calibration
* `PixelConfig`: container of per-pixel register parameters, namely `Enable`, `Hitbus`, `InjEn`, `TDAC`.

LocalDB stores the first two blocks as a normal `mongodb` document (searchable by e.g. the value of the field), while the pixel config part is stored in `GridFS` as a format of `python pickle` (i.e. document is not searchable by individual pixel's parameter value).

LocalDB book-keeps revision history of a chip config using `chip_config_api` in `module-qc-database-tools`. This can be seen as a simplified version of `git` version control.

A chain of chip config revision is identified by specification of `[ serialNumber, stage, branch ]` Three default branches of `warm`, `cold`, `LP` are prepared in each stage (`LP` stands for Low Power). Revision of each tag evolves independently.

A chip config revision contains the following information

* Chip's `GlobalConfig` and `Parameter` snapshot
* Link to `PixelConfig` data in `GridFS`
* Diff of config (except `PixelConfig`) with respect to the previous revision
* Message describing of what this revision is.
* Timestamp
* User tags (not commissioned)

An example of chip config revision is the following:

```JSON
{
  _id: ObjectId("644ca51098473364116d4088"),
  parent_revision_id: ObjectId("644c3d68a95172973d79354d"),
  config_data: {
    RD53B: {
      GlobalConfig: {
        AiRegionRow: 0,
        AuroraActiveLanes: 1,
        ...

        VrefRsensBot: 0,
        VrefRsensTop: 0
      },
      Parameter: {
        ADCcalPar: [ 5.894350051879883, 0.1920430064201355, 4990 ],
        ChipId: 15,
        ...
        VcalPar: [ 5.122000217437744, 0.210999995470047 ]
      }
    }
  },
  diff: '{"RD53B": {"GlobalConfig": {"InjVcalHigh": 770}}}',
  pix_config: {
    _id: ObjectId("6434d009dea088f081031da4"),
    md5: '7ad74c6c02f11c1cafe7e139d50d7a0d',
    chunkSize: 261120,
    length: Long("1241951"),
    uploadDate: ISODate("2023-04-11T03:12:09.353Z")
  },
  message: 'Submitted by YARR-dbAccessor (hostname: bowlfish, user: hoide)',
  timestamp: ISODate("2023-04-29T05:03:12.761Z"),
  tags: []
}
```

### Browsing chip config on LocalDB

If you navigate to a chip's page, at the bottom of the page there is an accordion block named "Config Revisions". If you click it, you can expand all config revisions of the chip of all branches in all stages.

![Config Revision](images/tech/config_revisions.png)

By clickng the Revision ID, you can also visually browse the config.

![Config Browser 1](images/tech/config_browser_1.png)

![Config Browser 1](images/tech/config_browser_2.png)

### Root config generation

For all stages after `INITIAL_WARM`, when the stage is incremented from the previous one, the `root` config is created in LocalDB for all 3 default branches in the background. Exact algorithm of this depends on the situation:

* If the current stage is `INITIAL_WARM`, the `root` config is synthesized from the measurement record of wafer probing. The same config is simultaneously pushed to ITkPD as an attachment under the chip. Hence, a chip record in ITkPD should always have 3 representative configs of `warm`, `cold`, `LP`.
* The config is revised by the tuning sequence or by other electrical tests. If the previous stage has the `TUNING` test, the config after the tuning of the previous stage is uploaded to ITkPD as an attachment under the chip, when that stage is signed off and synchronized with ITkPD. If the chip record on ITkPD has already the config of the same branch, it will be __overwritten by this process__. As a consequence of this, a chip record in ITkPD always has the latest 3 representative configs of `warm`, `cold`, `LP` at any stage after `INITIAL_WARM`.
* When the stage is incremented, the 3 representative configs are copied from ITkPD to LocalDB, and stored as `root` configs of the current stage.
