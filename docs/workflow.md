# Module QC Workflow

## Version information

This document is compatible with `localdb-tools` version `2.2` or later.

* The corresponding `YARR` version is `v1.4.4` or later.
* You need to deploy the following packages in your local DAQ system:
	* `module-qc-database-tools 2.0.2` or later
	* `module-qc-tools 2.0.0` or later
	* `module-qc-analysis-tools 2.0.0` or later

## tl;dr

The LBNL team provides the example [bash script to run the all chain of the electrical test of a stage](https://gitlab.cern.ch/atlas-itk/pixel/module/module-qc-database-tools/-/snippets/2631).

Some presentations are also available

* [Emily Thompson, Upgrade Week, May 10, 2023](https://indico.cern.ch/event/1223748/contributions/5393853/attachments/2644780/4577698/UpgradeMay2023_ModuleQCv2.pdf)
* [Kehang Bai, Module Group Meeting, May 25, 2023](https://indico.cern.ch/event/1289710/contributions/5419231/attachments/2653318/4596038/QC_V2_workflow_Kehang_Bai_module_meeting.pdf)

## Module Assembly

When a new module is assembled on your site, you need to register this new-born module to Production DB. We recommend __not to register a new module using ITkPD web page__, since in order to determine the module serial number, there are several rules that should be uniquely determined by the subcomponents' serial numbers.

LocalDB provides a dedicated interface to register the module for you by determining the serial number.

![Module Assembly Link](images/workflow/module_assemble_1.png)

You will be requested to input only the following informations:

* BareModule(s) serial number(s)
* PCB serial number
* Carrier serial number (optional)

![Module Assembly Link](images/workflow/module_assemble_2.png)

!!! note
    All subcomponents' location on Production DB must be your site (the site you are registered in Prodcution DB).

!!! note
    In order to assemble the module, BareModule needs to have all FE chips already.

!!! note
    All sub-components need to be not assembled previously.

 Once the module is successfully assembled, the module's information is downloaded to your LocalDB, and you are ready to go.

## Downloading Module

If the module assembly, or tests of the previous tests were carried out in the other site, you might not have the module in LocalDB though it is already registered in PDB. In this case, you should go to the top page of the LocalDB and click "Pull components from ITkPD" and follow the guide in the page in order to download the information of the module and sub-components, including the restults of previous stages.

![Module Download 1](images/workflow/module_download_1.png)

![Module Download 2](images/workflow/module_download_2.png)

!!! note
    It is not only a module you can download with this interface. Standalone sub-components of Sensor Tile, Bare Module, PCB are supported on top of Module.

!!! warning
    If your downloaded module's stage is not a valid one (e.g. `MODULETOPCB` which was deprecated), you need to switch the module's stage to the correct one by hand. You can do this by clicking the "Switch Stage" yellow button of the module's page.

!!! note
    Please do not switch the stage except the above reason, unless you understand the behavior e.g. for debugging purpose. You may end-up wiping previous stages test records at the worst case.

## Simple Electrical Tests

The following tests are the so-called simple electrical tests.

* ADC Calibration  (ADC_CALIBRATION)
* Analog Readback  (ANALOG_READBACK)
* SLDO  (SLDO)
* VCAL Calibration  (VCAL_CALIBRATION)
* Low Power Mode  (LP_MODE)
* Overvoltage Protection  (OVERVOLTAGE_PROTECTION)
* Undershunt Protection  (UNDERSHUNT_PROTECTION)
* Data Transmission  (DATA_TRANSMISSION)
* Injection Capacitance  (INJECTION_CAPACITANCE)

The term _simple_ means that the QC analysis possible to generate only from each measurement result (`RAW` record) as the input.

LocalDB calls the corresponding `module-qc-analysis-tools` analysis command to evaluate the measurement, and records the analysis result as a format compatible with ITkPD's `TestRun`. `RAW` measurement, as well as all plots and log files of the analysis become as an attachment of this analysis result document.

Running a simple electrical test is essentially just running 3 steps:

* __Step-1__: measurement itself
  
  ```bash
  measurement-"$measName" -c "$config" -m "$connectivity" -o "$output"
  ```
  
* __Step-2__: analyze the measurement (process within LocalDB)
  
  ```bash
  module-qc-tools-upload --path "${lastMeasDir}" \
  --host "$localdbHost" --port "$localdbPort" \
  --out "${anaResults}"
  ```
  
  Here, as you can see in the command option, interaction with LocalDB happens. The client (DAQ host) submits the measurement result, and the QC analysis program evaluates it inside LocalDB. As a response, LocalDB returns the result output `JSON` (TestRun format)
  
* __Step-3__: update the chip config of the DAQ host
  ```bash
  analysis-update-chip-config -i "$anaDir" -c "$moduleDir" -t "$qctype"
  ```
  Using the result of the analysis, the config file is revised __locally__ within the DAQ host.

The [bash script](https://gitlab.cern.ch/atlas-itk/pixel/module/module-qc-database-tools/-/snippets/2631) embeds these steps and loop over Tests.

!!! note
    The chip config inside LocalDB is also revised reflecting the analysis output when `module-qc-tools-upload` is executed.

## YARR Chip Scans

!!! note
    Before going through this section, user is expected to have a basic idea of how the chip config is administrated in LocalDB. If you are not familiar with, please go through [the doc](../technicalities/#chip-configs).

YARR chip scans are simply possible to carry out by the `scanConsole` command. However, initially you need to generate the scan connectivity and chip config files using `module-qc-database-tools`.

### Check LocalDB

Before starting anything about YARR scan, you need to make sure the presence of the module in LocalDB, and the QC stage is exactly the one you are going to test.

!!! note
    Immediately after signing off from the previous stage, the LocalDB might not be ready to host any new test. If this is the case, when you navigate to LocalDB's module page, you will see a warning message like below.
    ```
    [ WARNING_FE_CONFIG_NOT_READY ]
    20UPGR91301046: WARNING: FE Chip configs for the current stage (MODULE/INITIAL_COLD) is not ready
    ```
    Once the preparation of the stage is done, this warning message should disappear automatically. If this is not the case, something is wrong with LocalDB.

### Initial Generation of configs

The config needs to be deployed to the local DAQ host __every beginning of the stage__.

```bash
generateYARRConfig --sn 20UPGR91301046 \
--accessCode1 YOUR_ACCCESS_CODE_1 \
--accessCode2 YOUR_ACCESS_CODE_2 \
--outdir <the_dir_to_store_config>
```

### Running scans

Running scans is pretty standard as usual `scanConsole`:

```bash
cd path/to/yarr
./bin/scanConsole -r configs/controller/specCfg-rd53b-16x1.json \
-c 20UPGR91301046/20UPGR91301046_L2_warm.json \
-s configs/scans/rd53b/std_digitalscan.json \
-W MHT
```

An important option to be added to store the output result to LocalDB properly is the `-W` option. This option can append tags associated to the scan, which is convenient in the later phase when we carry out complex test analyses, as we will see later.

Three default tags are appointed by default:

* `MHT` for Minimal Health Test
* `TUN` for Tuning
* `PFA` for Pixel Failure Analysis

On top of that, user can specify arbitrary user's tag as needed. The syntax of specifying multiple tags is as follows:

```bash
-W "['my temporary test', 'DEBUG', 'MHT']"
```

!!! warning
    Please avoid to use single or double quotations as a part of the tag name.

With `-W` option, not only the scan outputs like occupancy map, the chip config is also uploaded to LocalDB

### Browsing scan results

The easiest way to check the latest YARR scan is to click the "YARR Scan" in the top navigation bar.

![YARR Navigation 1](images/workflow/yarr_navigate_1.png)

Then you can see the latest YARR scans in the reverse chronological order:

![YARR Navigation 1](images/workflow/yarr_navigate_2.png)

By clicking "View" button, you can see the detail of the scan, including plots:

![YARR Scan 1](images/workflow/yarr_scan_1.png)
![YARR Scan 2](images/workflow/yarr_scan_2.png)

### Required scans

!!! note
    The example [bash script](https://gitlab.cern.ch/atlas-itk/pixel/module/module-qc-database-tools/-/snippets/2631) is provided by the LBNL group. The most accurate and up-to-date specification of electrical tests is documented in [this repository](https://gitlab.cern.ch/atlas-itk/pixel/module/itkpix-electrical-qc).

* Minimal Health Test
	* `std_digitalscan`
	* `std_analogscan.json`
	* `std_thresholdscan_hr`
	* `std_totscan` with `-t 6000` option
* Tuning
	* `std_thresholdscan_hr`
	* `std_totscan` with `-t 6000` option
	* `std_tune_globalthreshold` with `-t 2000` option
	* `std_tune_pixelthreshold` with `-t 2000` option
	* `std_retune_globalthreshold` with `-t 1500` option
	* `std_retune_pixelthreshold` with `-t 1500` option
	* `std_thresholdscan_hd`
	* `std_totscan`
* Pixel Failure Analysis
	* `std_digitalscan` with `-m 1` option
	* `std_analogscan`
	* `std_thresholdscan_hd`
	* `std_noisescan`

## Registration of Complex Electrical Tests

A complex electrical test can be reigstered to LocalDB using the browser, once you completed the entire list of scans required for the test.

From the Front-end chip's page, or the Module list table, you can access to the chip's page.

When you scroll down, you find an orange table listing Tests of the stage.

If you sign-in to LocalDB, you will find a green button "Checkout Scans" at the column "Action" for Minimal Health Test, Tuning or Pixel Failure Analysis.

Pushing this button, you will be guided to register the set of YARR scans to evaluate the Test.

The tag you specified during the YARR scan, like `MHT`, `TUN`, or timestamp or run number will hint you which scan you will need to register here.

!!! note
    But these are only guides up to the end, the user needs to take the responsibility of the validity of the scan results.

!!! note
    Do not worry if you miss to register wrong scans, you can create more than one complex analysis result until you meet the correct registration.

!!! note
    You can skip to register some scans if you do not have or you could not take those scans. The analysis can run without these scans entry. Of course, the result of the analysis is most likely failure.

!!! note
    Although you register scans chip-by-chip, you can specify the same set of scan runs for all chips as common. In order to do this, you just need to check the checkbox at the bottom of the page. This checkbox is actually toggled on by default. However, if you have reasons to select runs for individual chips, you can do.

At the end of registration, the analysis script runs on the server side, and the QC pass/fail is judged for the test.

## Electrical Test Summary

For each stage requiring electrical test, a summary record test called "E-Summary" is allocated. This is the place to aggregate all FE-level test results and make summary e.g. total bad pixels of the module. The user interface is similar to the above Complex Electrical Tests case, but you are guided to register the record of Minimal Health Test or Tuning instead of registering YARR scans.

!!! note
    Do not confuse Module-level tests with FE-level tests. Electrical Summary Test is a Module-level test, while Minimal Health Test, Tuning, etc. are FE-level tests.

## Stage Sign-off

Once all tests of the stage are filled, you can proceed signing-off the stage.

!!! note
    Actually you can sign-off without missing some tests, as this is a commissioning phase and not all tests are ready to serve.

You click the blue "Stage Sign-off" button under the module's page. In the next page, you are required to select one result per tests (there can be multiple candidate results if you have done multiple times).

!!! note
    Do not confuse Module-level tests with FE-level tests. Module-level tests include E-Summary and various non-electrical tests.

Once you selected all results, you can proceed to do sign-off.

!!! note
    Sign-off has technically two steps. The first step is __Local Sign-off__ where the stage is only incremented locally within the LocalDB. The second step is __Push to Production DB__ and the results are synchronized to Production DB after this second step.

## Non-electrical Tests

Non-electrical tests are not commissioned yet in LocalDB `v2.2`. A few preview tools are prepared, but these should not be regarded as full commissioned.