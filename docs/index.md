# Top Page

## General

The `LocalDB` is an intermediate aggregation place of the data of a QC site/cluster for module QC during the production, designed to support concurrent production and tests of ITk modules over various sites.

It aggregates and organizes all electrical and non-electrical test results sourced from various PCs or devices in the lab. LocalDB supports module QC and its pre-defined tests and stages, which are parts of the waterflow of the production defined in the `ITk production DB`. It refers to the ITk production DB to load or generate module/FE serial numbers. 

For electrical scans, it runs a high-level analysis process (e.g. bad pixel analysis) to synthesize a suitable format to upload to the production DB. The site administrator can select adequate scans to be submitted to production DB.

The LocalDB can download test results of a module from the production DB and can offer to compare the results between different stages over sites. In this view, typically it is natural to allocate one LocalDB service per site in our production model.

## Overall Scheme
![SW_Structure](images/qc-flow/Demo_sw.png)

* `Production DB`: A central DB for ITk,setup in Czech.
* `LocalDB viewer`: A web application to manage module QC.
* `QC-helper`: A GUI to upload non-electrical test.

## MongoDB

The backend database service of the LocalDB system is `MongoDB`. It is highly flexible for storing JSON-like NoSQL documents, but it also enables to relate documents. `LocalDB` makes use of `MongoDB` features effectively. The PC should allocate up to several TB of the disk storage for the `MongoDB` service, assuming around 1GB is consumed per module for storing the tests of all stages. The necessary capacity would depend on the number of modules produced at the site. The `LocalDB` data can be synchronized to an independent `MongoDB` service to support backup.


!!! Note
    For `CentOS7` installation, `MongoDB v4.2.21` is installed by default.
    We have not testest `MongoDB v5` from the developer side.

## QC tests and LocalDB

Two frameworks exist to upload test data to `LocalDB`.

- The `YARR` software, which is used for controlling ITkPix FEs and running scans, is native to `LocalDB` to upload scans.

- In order to support uploading generic non-electrical scans, e.g. IV scans, metrology, optical inspection etc., the `QCHelper` GUI (based on `PyQt5`) is the central interface. It interacts with `LocalDB` to properly format all non-electrical test results under a generic `JSON` format and upload them to `LocalDB` as tests.

For downloading results and configurations of scans to a local host, the `YARR` software contains the so-called `LocalDB retriever` CLI. In addition, the `LocalDB Viewer` browser provides various buttons to download specific data manually.

## Handling of time-series data in LocalDB

In parallel to electrical scans, the DCS time-series data (e.g. temperature, current, voltage) needs to be associated as an auxiliary information. We generally assume `influxDB` is the default system to store such a time-series data monitored in the lab. However, `LocalDB` itself does not store the time-series data: only a clipped *snapshot* of such data can be associated to each electrical scan. Such clipping task is not done within the `LocalDB` server side, but it is a task to be done as a post-process of `YARR` scans.

!!! Warning
    The underlying data scheme between `InfluxDB 1.x` and `InfluxDB 2.x` is different and not compatible. The current `LocalDB` system is only adapted to `InfluxDB 1.x`. Inclusion of `InfluxDB 2.x` is a future ToDo.

## Host specs

This service host can be a standalone workstation. Typically we assume the same host is used as an `HTTP` server of `LocalDB Viewer`, the user's browser interface frontend for module QC production operation. For smoother operation, the admin should make sure accessibility to this `HTTP` service from other hosts of the site, or if required, from other sites.

Though it is possible, we do __not__ generally assume that the `LocalDB` host equips the `YARR` hardware. In this documentation we assume the `LocalDB` host is a separate host PC from those used to run scans.

## OS support

We officially support `CentOS7` and `CentOS Stream 8` Linux system in this document as the default operating systems. One should be able to install the service to `macOS`, but it should be the user's responsibility to setup.
